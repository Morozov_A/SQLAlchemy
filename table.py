from sqlalchemy import (
    Table,
    MetaData,
    create_engine,
    Column, # Для создания колонок
    Integer,#Типы данных
    String,#Типы данных
    Boolean,#Типы данных
)

engine = create_engine("sqlite:///example-table.db", echo=True) #Для подключения, путь sqlite:   echo=True для инфо о том что мы сделали
metadata = MetaData() #Инициализируем метадату

users_table = Table( #Создаем таблицу
    "users",
    metadata,#Указываем позиционные аргументы
    Column("id", Integer, primary_key=True), #Создаем колонки  Primmary_key = основной ключ,главный. "ID" уникальное значение на которое ориентируется БД при работе с этой строкой
    Column("username", String(32), unique=True),# "Имя" - строкой(макс 32 символа), unique=Tru чтобы не повторялась строчка
    Column("is_staff", Boolean, nullable=False, default=False, server_default="0"), #"Ктото из админ. сайта" nullable=False значит не может означат none,default=Falseвсеи новым записям будет присваит.
) #Boolean - может быть любым True/False/none server_default="0"


if __name__ == "__main__":
    metadata.create_all(engine)#движок для создания таблицы
