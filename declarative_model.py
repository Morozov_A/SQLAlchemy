from datetime import datetime # Для времени точного
from sqlalchemy import (
    create_engine,
    Column,
    Integer,
    String,
    Boolean,
    DateTime,#Для created_at
    func, #Для определения now
)
from sqlalchemy.ext.declarative import declarative_base #

engine = create_engine("sqlite:///example-table.db", echo=True)
Base = declarative_base(bind=engine) # Создаем базу bind=engine для проброса в методату


class User(Base): # создаем класс
    __tablename__ = "users" #Имя таблицы

    id = Column(Integer, primary_key=True)#Все те же колонки, но это свойства этого класса
    username = Column(String(32), unique=True)
    is_staff = Column(Boolean, nullable=False, default=False, server_default="0")
    created_at = Column(DateTime, default=datetime.utcnow, server_default=func.now())#Для указа времени УТС default=datetime.utcnow server_default=func.now()Для времени сейчас в SQLite нет такой функ.


if __name__ == "__main__":
    Base.metadata.create_all(engine)
