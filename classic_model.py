from sqlalchemy import (
    Table,
    MetaData,
    create_engine,
    Column,
    Integer,
    String,
    Boolean,
)
from sqlalchemy.orm import mapper #Связывает табл. с моделью

engine = create_engine("sqlite:///example-table.db", echo=True)
metadata = MetaData()

users_table = Table(
    "users",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("username", String(32), unique=True),
    Column("is_staff", Boolean, nullable=False, default=False, server_default="0"),
)


class User: # Клас пользоваетль
    def __init__(self, id: int, username: str, is_staff: bool): #Обьявляем инициализатор
        """
        :param id:
        :param username:
        :param is_staff:
        """
        self.id = id # Для сохранения в модель
        self.username = username
        self.is_staff = is_staff


# user = User(id=1, username="guest", is_staff=False) #Будет создан новый пользователь
# user.is_staff = True # Даем права для администр.

mapper(User, users_table) #связывает их для работы в ОРМ
